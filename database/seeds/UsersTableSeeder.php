<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::insert([
            'name' => 'Carlos Andres',
            'lastname' => 'Beltran Franco',
            'email' => 'carlosbeltran@ingenieros.com',
            'password' =>  bcrypt('12345'),
            'doc_types_id' => 1,
            'document'=> '1110487813',
            'pic'=> 'images/profile_pic/nouser.png',
        ]);

        \App\Models\User::insert([
            'name' => str_random(10),
            'lastname' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' =>  bcrypt('12345'),
            'doc_types_id' => rand(1,3),
            'document'=> rand(1000000000, 1999999999),
            'pic'=> 'images/profile_pic/nouser.png',
        ]);



        \App\Models\User::insert([
            'name' => str_random(10),
            'lastname' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' =>  bcrypt('12345'),
            'doc_types_id' => rand(1,3),
            'document'=> rand(1000000000, 1999999999),
            'pic'=> 'images/profile_pic/nouser.png',
        ]);



        \App\Models\User::insert([
            'name' => str_random(10),
            'lastname' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' =>  bcrypt('12345'),
            'doc_types_id' => rand(1,3),
            'document'=> rand(1000000000, 1999999999),
            'pic'=> 'images/profile_pic/nouser.png',
        ]);

    }
}

