<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname');
            $table->string('email', 100)->unique();
            $table->string('password');
            $table->integer('doc_types_id')->unsigned();
            $table->string('document', 100)->unique();
            $table->string('pic')->default('images/profile_pic/nouser.png');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

        });

        Schema::table('users', function($table) {
            $table->foreign('doc_types_id')->references('id')->on('doc_types')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
