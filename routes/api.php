<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Auth
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::get('logout', 'AuthController@logout');
});
Route::middleware(['api', 'jwt'])->group(function () {
    Route::group([
        'prefix' => 'auth'
    ], function () {
        Route::get('me', 'AuthController@me');
        Route::get('refresh', 'AuthController@refresh');
        Route::get('invalidate', 'AuthController@invalidate');
    });

//Admin
    Route::group([
        'prefix' => 'admin'
    ], function () {
        Route::get('document_types', 'DocumentTypesController@index');
        Route::apiResource('user', 'UserController');
        Route::post('plain', 'PlainController@store');
        Route::get('plain', 'PlainController@index');
    });
});

