<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User as User;
use Illuminate\Http\Request;


class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = array('email' => $request->email, 'password' => $request->password);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized. Check Credentials']);
        }

        $Auth_user = auth()->user();
        $MyUser = User::find($Auth_user->id )->with('DocType')->first()->toArray();
        $response = array();
        $response = $MyUser;
        $response['pic'] = asset('/images/profile_pic/' . $MyUser["pic"]);
        $response["expires_in"] = auth()->factory()->getTTL() * 60;
        $response["accessToken"] = $token;
        $response["refreshToken"] = $token;

        return response()->json($response);

    }


    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $Auth_user = auth()->user();
        $MyUser = User::find($Auth_user->id )->first();
        $MyUser->DocType;
        $response = array();
        $response['user']=$MyUser;
        $response['pic'] = asset('/images/profile_pic/' . $MyUser->pic);

        return response()->json($response);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    public function invalidate()
    {
        auth()->invalidate();

        // Pass true as the first param to force the token to be blacklisted "forever".
        auth()->invalidate(true);

        return response()->json(['message' => 'Token Invalidated successully.']);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
