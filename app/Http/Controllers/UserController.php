<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserCollection;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use PhpParser\ErrorHandler\Collecting;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = User::with('DocType')->get();

        return new UserCollection($item);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            "name" => "required|max:255",
            "lastname" => "required|max:255",
            "email" => "required|unique:users|max:100",
            "password" => "required|confirmed",
            "doc_types_id" => "required|integer",
            "document" => "required|unique:users|max:100",
            "pic" => "required|file|max:5120|mimes:jpg,gif,jpeg,png"
        ]);

        $new_name = time() . '.' . $request->pic->extension();
        $path = $request->pic->storeAs('images/profile_pic', $new_name, 'public');
        $data["pic"] = $path;

        $InsertId = User::insertGetId($data);
        $inserted = User::where("id", $InsertId)->get();

        return response()->json($inserted);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $_user = current($user->get()->toArray());
        $_user['pic'] = asset('/images/profile_pic/' . $_user['pic']);
        return response()->json($_user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        $data = $request->validate([
            "name" => "required|max:255",
            "lastname" => "required|max:255",
            "email" => "required|unique:users|max:100",
            "password" => "nullable|confirmed",
            "doc_types_id" => "required|integer",
            "document" => "required|unique:users|max:100",
            "pic" => "nullable|file|max:5120|mimes:jpg,gif,jpeg,png"
        ]);

        if ($request->post('password') == '') {
            unset($data['password']);
         }

        if (!empty($request->file("pic"))) {
            $new_name = time() . '.' . $request->pic->extension();
            $path = $request->pic->storeAs('images/profile_pic', $new_name, 'public');
            $data["pic"] = $path;
        }


        $user->update($data);

        return response()->json($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $item = $user->delete();

        $response["deleted"] = $item;
        if ($item) {
            $response["status"] = 200;
        } else {
            $response["status"] = 401;
        }

        return response()->json($response);
    }


}
