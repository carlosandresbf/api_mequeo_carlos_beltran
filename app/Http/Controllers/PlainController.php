<?php

namespace App\Http\Controllers;

use App\Http\Resources\GlobalCollection;
use App\Models\Plain;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PlainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = Plain::with('User')->get();

        return new GlobalCollection($item);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            "plain_file" => "required|file|max:5120|mimes:txt,csv"
        ]);

        $new_name = time() . '.' . $request->plain_file->extension();
        $path = $request->plain_file->storeAs('plain_files', $new_name, 'public');

        $plain_file = File::get($path);
        $data = array();


        foreach (explode("\r\n", $plain_file) as $key=>$line){
            $line = trim($line);
            //$data['string'][$key] = $line;
            if (preg_match_all("/[A-Z]/", $line, $matches )) {
                $data['newString'][$key]=  $matches[0];
            }
            (isset($data['newString'][$key]))? $data['cant'][$key]= count($data['newString'][$key]) : $data['cant'][$key]= 0;

        }

        $max_item = array_keys($data['cant'], max($data['cant']));

        $string_to_save = array_reduce($data['newString'][ $max_item[0] ],
                                        function($carry, $item){
                                                    $carry .= $item;
                                                    return $carry;
                                            });

        $Auth_user = auth()->user();
        $save['content'] = $string_to_save;
        $save['users_id'] = $Auth_user->id;
        $InsertId = Plain::insertGetId($save);

        $return['message']="Insertado";
        $return['id']=$InsertId;
        return response()->json($return);
    }

    //Reduce el array de caracteres en Mayuscula a un solo string

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Plain $plain
     * @return \Illuminate\Http\Response
     */
    public function show(Plain $plain)
    {
        return response()->json($plain);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Plain $plain
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Plain $plain)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Plain $plain
     * @return \Illuminate\Http\Response
     */
    public function destroy(Plain $plain)
    {

    }


}
