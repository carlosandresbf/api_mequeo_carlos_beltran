<?php

namespace App\Http\Controllers;

use App\Models\DocType;
use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;

class DocumentTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = DocType::get();

        return new GlobalCollection($item);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DocType  $docType
     * @return \Illuminate\Http\Response
     */
    public function show(DocType $docType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DocType  $docType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocType $docType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DocType  $docType
     * @return \Illuminate\Http\Response
     */
    public function destroy(DocType $docType)
    {
        //
    }
}
