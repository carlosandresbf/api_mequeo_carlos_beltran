<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    use SoftDeletes;
    protected $table = "users";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'lastname', 'email', 'password','password_confirmation', 'doc_types_id', 'document', 'pic'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     *//*
    protected $hidden = [
        'UserPassword'
    ];
*/
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function DocType()
    {
        return $this->belongsTo('App\Models\DocType', 'doc_types_id', 'id');
    }


    public function News()
    {
        return $this->hasMany('App\Models\News', 'users_id', 'id');

    }



}
