<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocType extends Model
{
    use SoftDeletes;
    protected $table = "doc_types";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'abb'];

    public function Users()
    {
        return $this->hasMany('App\Models\User', 'doc_types_id', 'id');

    }
}
