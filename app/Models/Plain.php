<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plain extends Model
{
    use SoftDeletes;
    protected $table = "plain";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = ['content', 'users_id'];
    protected $dates = ['deleted_at'];

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'users_id', 'id');

    }

}
